<?php

ini_set('display_errors', true);

if (!empty($_GET['timer'])) {
	function _getMicrotime() {
        return array_sum(explode(' ', microtime()));
	}
	$_timer = _getMicrotime();
}

try {
 
    require_once('./kernel.php');
    Kernel::getInstance()->boot();
    Zend_Controller_Front::getInstance()->dispatch();
    
} catch (Exception $e) {
	Kernel::getInstance()->displayError($e);
}

if (!empty($_GET['timer'])) {
	echo '<br /><br /><hr />';
	echo '<pre>>>> Page took ' . (_getMicrotime() - $_timer) . 's to load</pre>';
	die();
}

?>