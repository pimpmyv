<?php

class ApiController extends Zend_Controller_Action {

	public function init() {
		$this->_helper->viewRenderer->setNoRender();
	}

	public function indexAction() {
	
	}
	
	public function testAction() {
		echo $this->results(array($this, 1, 3, 5));
	}
	
	private function results($res) {
		$K = Kernel::getInstance();
		
		$returnType = $K->VARS['sub0'] ? $K->VARS['sub0'] : 'json';
		switch ($returnType) {
			case 'plain':
				return __($res, true);
			case 'json':
				return json_encode($res);
			case 'jsonpretty':
				return '<pre>' . $this->indentJson(json_encode($res)) . '</pre>';
		}
	}
	
	/* from http://fr.php.net/manual/en/function.json-decode.php */
	private function indentJson($str){
		$strOut = '';
		$identPos = 0;
		for ($loop = 0;$loop<= strlen($str) ;$loop++) {
			$_char = substr($str,$loop,1);
			//part 1
			if($_char == '}' || $_char == ']') {
				$strOut .= chr(13);
				$identPos --;
				for($ident = 0;$ident < $identPos;$ident++){
					$strOut .= chr(9);
				}
			}
			//part 2
			$strOut .= $_char;
			//part 3
			if($_char == ',' || $_char == '{' || $_char == '['){
				$strOut .= chr(13);
				if($_char == '{' || $_char == '[') $identPos ++;
				for($ident = 0;$ident < $identPos;$ident++){
					$strOut .= chr(9);
				}
			}
		}
		return $strOut;
	}
	

}

?>