<?php

class Href {
	
	public static function apply($url) {
		$K = Kernel::getInstance();

		# first char modifier
		$murl = substr($url, ($url[1] == '/') ? 2 : 1, strlen($url));
		switch ($url[0]) {
			case ';':
				$url = $K->infos['httphost'] . $murl;
				break;
			case ($url[0] == '/') || ($url[0] == ':'):
				$url = $K->infos['httphosti18n'] . $murl;
				break;
		}

		if (strpos($url, "http") !== 0) {
			$url = $K->infos['httphosti18n'] . $url;
		}

		return Zend_Uri::factory($url)->getUri();
	}
	
}

?>