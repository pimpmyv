<?php

# debug function
function __($obj, $ret=false) {
	if ($ret) {
		ob_start();
	}
    if (!extension_loaded("xdebug")) {
    	Zend_Debug::dump($obj);
    } else {
    	var_dump($obj);
    }
    if ($ret) {
    	return ob_get_contents();
    }
}

# redirect function
function redirect($url) {
	header('Location: ' . $url);
	die();
}

# kernel class
class Kernel {

	private static $_instance = null;

	public $db = null;
	public $debug = null;
	public $config = null;
	
	public $VARS = array();
	public $infos = array();
	public $site = null;
	public $i18n = null;

	public function __construct() {
		$h = $_SERVER['HTTP_HOST'];
		
		$this->infos = array(
			"host" 			=> $h,
			"dest" 			=> 'live',
			"httphost"		=> "http://".$h."/"
		);
	}
	
	public static function getInstance() {
		if (self::$_instance == null) {
			self::$_instance = new Kernel();
		}
		return self::$_instance;
	}
	
	public function boot($bootOrder = array()) {
		$this->initVars();
		
		if (empty($bootOrder)) {
			$bootOrder = array('PHPConfig', 'IncludePath', 'Zend', 'Domain', 'Config', 'DB', 'Site');
		}
		foreach ($bootOrder as $fn) {
			$fn = 'init' . $fn;
			$this->$fn();
		}
		
		# register infos in the view
		self::getView()->infos = $this->infos;
	}
	
	public static function getView() {
		return Zend_Layout::getMvcInstance()->getView();
	}
	
	private function initDomain() {

		# determine domain or redirect
		if (!empty($this->VARS['domain'])) {
			$_SERVER['REQUEST_URI'] = str_replace('/'.$this->VARS['domain'], '/', $_SERVER['REQUEST_URI']);
		} else {
			$url = '/';

			$this->locale = new Zend_Locale(Zend_Locale::BROWSER);
			$defaultDomain = $this->locale->getLanguage();

			if (empty($this->VARS['controller']) || (false === strstr($_SERVER['REQUEST_URI'], $this->VARS['controller']))) {
				$url = $_SERVER['REQUEST_URI'] . $defaultDomain . '/';
			} else {
				$url = str_replace('/'.$this->VARS['controller'].'/', '/'.$defaultDomain.'/'.$this->VARS['controller'].'/', $_SERVER['REQUEST_URI']);
			}
			
			redirect($url);
		}

		# locale and i18n
		$this->locale = new Zend_Locale(Maps::$langlocale[$this->VARS['domain']]);
		$moPath = 'resources/locale/'. $this->locale->getLanguage() . '_' . $this->locale->getRegion() .'/LC_MESSAGES/common.mo';
		$this->i18n = new Zend_Translate('gettext', $moPath, $this->locale);
		
		# fill info
		$this->infos['domain'] 			= $this->VARS['domain'];
		$this->infos['httphosti18n'] 	= $this->infos['httphost'].$this->VARS['domain']."/";
		$this->infos['locale'] 			= $this->locale->getLanguage() . '_' . $this->locale->getRegion();
		
	}
	
	private function initPHPConfig() {
		# Be sure that you're showing all errors
		ini_set('display_errors', true);
		ini_set('error_reporting', E_ALL | E_STRICT);
		
		# timezone is always a problem when not set
		ini_set('date.timezone', 'Europe/Paris');
		
	}
	
	private function initIncludePath() {
	    $includePath = array('.', './site/models/');
	    
	    $libDir = './lib/';
	    foreach(new DirectoryIterator($libDir) as $value) {
	        $path = $libDir . $value;
	        if (is_dir($path) && (!in_array((string)$value,array('.','..')))) {
	            $includePath[] = $path;
	        }
	    }

	    set_include_path(get_include_path() . PATH_SEPARATOR . implode(PATH_SEPARATOR, $includePath));
	}
	
	private function initZend() {
		# load the loader
		require_once('Zend/Loader.php');
		Zend_Loader::registerAutoload();

		# session start
		Zend_Session::start();

		# controller configuration
		$controller = Zend_Controller_Front::getInstance();
		$controller->setControllerDirectory('./site/controllers/');
		$controller->setDefaultControllerName('Index');
		$controller->throwExceptions(true);
	 
		# views param
		$view = new Zend_View();
		$view->setEncoding('UTF-8');
		$view->setScriptPath('./site/views/Pages/');
		$view->setHelperPath('./site/views/Helpers/');

		# modify the viewRenderer default behaviour
		$viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
		$viewRenderer->setView($view);
		$viewRenderer->setViewSuffix('tpl');
		
		# use the laout object
		$layout = Zend_Layout::startMvc();
		$layout->setLayout('default');
		$layout->setView($view);
		$layout->setLayoutPath('./site/views/Layouts/');
		$layout->setViewSuffix('tpl');
		# todo follow if Zend_Layout use inflector from the view renderer in next relases
	}
	
	private function initVars() {
		$this->VARS = array();
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
			$this->VARS['_ajax'] = true;
        }
		if (isset($_SESSION)) {
			$this->VARS = array_merge($this->VARS, $_SESSION);
		} 
		$this->VARS = array_merge($this->VARS, $_COOKIE, $_GET, $_POST);
	}
	
	private function initConfig() {
		$this->config = new Zend_Config_Ini('config.ini', $this->infos['dest']);
		
		$this->infos['contactmail'] = $this->config->contactmail;
	}
	
	private function initDB() {
		$dbtype = $this->config->db->type;
		$params = array(
			'host' => $this->config->db->host,
			'username' => $this->config->db->user,
			'password' => $this->config->db->password,
			'dbname' => $this->config->db->name
		);
		
		$this->db = Zend_Db::factory($dbtype, $params);
	}

	private function initSite() {
		$this->site = new Site();
	}

	public static function displayError(Exception $e) {
		error_log($e->getMessage());
		
		echo '<!-- ERROR -->';
		echo '<style type="text/css">';
		echo '#mainPanelError {';
		echo '  text-align:left;';
		echo '  padding:10px;';
		echo '  background:#ffea50;';
		echo '  border:4px double red;';
		echo '}';
		echo '#stackTrace {';
		echo '  text-align:left;';
		echo '  background:white;';
		echo '  border:1px solid black;';
		echo '  margin:30px;';
		echo '}';
		echo '.stackTraceElement {';
		echo '  text-align:left;';
		echo '  background:white;';
		echo '  margin:0 0 5px 0;';
		echo '}';
		echo '.fileInfo {';
		echo '  font-size:0.8em;';
		echo '  color:#999999;';
		echo '  margin:0 0 0 15px;';
		echo '}';
		echo '</style>';
		echo '<div id="mainPanelError"><strong>';
		echo get_class($e);
		echo '<br />';
		echo $e->getMessage();
		echo '</strong>';
		$trace = $e->getTrace();
		if ($trace) {
			function getInfo($traceElt, $param = false) {
				$info = array_key_exists('class', $traceElt) ? $traceElt['class'] . $traceElt['type'] : '';
				$info .= $traceElt['function'];
				$info .= '(';
				if ($traceElt['args']) {
					$count = count($traceElt['args']);
					for ($i = 0; $i < $count; $i++) {
						$arg = $traceElt['args'][$i];
						$info .= is_object($arg) ? get_class($arg) : gettype($arg);						
						if ($i < $count - 1) {
							$info .= ', ';
						}
					}
				}
				$info .= ')';
				if ($traceElt['args'] && $param) {
					$info .= __($traceElt['args'], 'func_get_args(): ', false);
				} 
				return $info;
			}
			echo '<br /><em>in '.getInfo($trace[0], true).'</em>';
			echo '<div id="stackTrace">';
			echo '<b>StackTrace :</b><br /><br />';
			foreach ($trace as $k => $line) {
				echo '<div class="stackTraceElement">';
				$callInfo = ($k < count($trace) - 1) ? getInfo($trace[$k + 1]) : "";
				echo $callInfo;
				if (array_key_exists('file', $line)) {
					echo ' (' .basename($line['file']) .':'.$line['line'].')<br />';
					echo '<span class="fileInfo">in file '.$line['file'].'</span>';
				}
				echo '</div>';
			}
			echo '<div>';
		}
		echo '</div>';
	}
	
}

?>