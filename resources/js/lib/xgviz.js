

var xGviz = Class.create({
	
	initialize: function() {
		this.activeSerials={};
		this.ajaxSemaphore=0;
		
		this.baseUrl = '/xgviz';
	},
	
	log:function(msg) {
		var log = this.debug;
		if (Prototype.Browser.Gecko) {
			try {
				//use firebug if available
				console.log(msg);
				log = false;
			} catch (e) {
			}
		} 
		
		if (log) { //IE and others 
			if (tool.debug) {
				//build a div to log
				var div = $('xGviz_log');
				if (!div) {
					var body = document.getElementsByTagName('body')[0];
					body.innerHTML = '<div id="xGviz_log"></div>' + body.innerHTML;
					var div = $('xGviz_log');
					div.setStyle('position:absolute;bottom:0;right:0;background:lightyellow;border-top:4px double red;height:120px;width:100%;overflow:auto;');
				}
				
				//log
				div.innerHTML = msg + '<br />' + div.innerHTML;
			}
		}
	},
	
		//Generic ajax request.
	ajaxRequest:function(options) {
	
		//default options
		options=Object.extend({
			"url":"",
			"params":{},
			"method":"post",
			"callback":false,
			"tryback":function() {return true},
			"errback":false,
			"serial":true,
			"timeout":7000,
			"timeoutId":false, //do not overload
			"timeouted":false,  //do not overload
			"retryDelay":3000,
			"retryCount":5
		},options);
		

		if (options.serial) {
			options.serial=(++this.nextSerial);
			this.activeSerials[options.serial]=true;
		}
		
		var request = new Ajax.Request(options.url,{
		
			method:options.method,
			parameters:options.params,
			
			onFailure:function() {
				
				clearTimeout(options.timeoutId);
				delete this.activeSerials[options.serial];
								
				//Should we continue to try the request?
				if ((--options.retryCount)>=0 && options.tryback(options.retryCount)) {
					//fail silently. ajaxSemaphore is going to get incremented again
					this.ajaxSemaphore--;
					setTimeout(function() {this.ajaxRequest(options);}.bind(this),options.retryDelay);
				} else {
					if (options.errback) options.errback();
				}
				
			}.bind(this),
			
			onSuccess:function(transport) {
				clearTimeout(options.timeoutId);

				if (!options.serial || (options.serial in this.activeSerials)) {
					delete this.activeSerials[options.serial];

					if (options.callback) {
						try {
							options.callback(transport.responseText,transport);
						} catch (e) {
							tool.log("error in callback: "+e.name+" => " + e.message);
						}
					}
				}
				
			}.bind(this)
		});
		
		//http://ajaxblog.com/archives/2005/06/01/async-requests-over-an-unreliable-network
		if (options.timeout) {
			options.timeoutId=setTimeout(function() {
				options.timeouted=true;
				if (request.transport.readyState>0 && request.transport.readyState<4) {
					request.options['onFailure'](request.transport, request.json);
				}
			},options.timeout);
		}
		
		return {"options":options,"serial":options.serial,"abort":function() {
			this.ajaxAbortRequest(options.serial);
		}.bind(this)};
	},
	
	jsonRequest:function(url,params,callback,options) {
		options=Object.extend(options||{},{
			"callback":function(responseText) {
				if (callback) {
					callback(responseText.evalJSON());
				}
			},
			"params":params||{},
			"url":url
		});
		
		return tool.ajaxRequest(options);
	},
	
	api:function(method,params,callback,errback,options) {
		try {
			return this.jsonRequest(this.baseUrl + "/api/"+method+"/json",params,callback,options);
		} catch (e) {
			tool.log(e);
			if (errback) errback();
		}
	}
	
});

var tool = new xGviz();