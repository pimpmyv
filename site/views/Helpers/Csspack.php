<?php

class Zend_View_Helper_Csspack {

	public function csspack($name = null, $force = null) {
		if (!$name) return '';
		if ($force) $name = $force;
	
		$packs = array(
			'default' => array(
				'media' => 'all',
				'resources' => array('css/typography.css')
			)
		);

		$layout = Zend_Layout::getMvcInstance()->getLayout();
		$layout = 'css/layouts/' . $layout . '.css';
		if (!is_file('resources/' . $layout)) {
			$layout = null;
		}
	
		if (!in_array($name, array_keys($packs))) return '';

		if ($layout) {
			# todo array_prepend
			$packs[$name]['resources'] = array_merge(array('css/layouts/common.css', $layout), $packs[$name]['resources']);
		}
		
		foreach (array('elements', 'pages') as $categ) {
			foreach(new DirectoryIterator('./resources/css/' . $categ . '/') as $css) {
		        if (!in_array((string)$css,array('.','..'))) {
					$resource = 'css/' . $categ . '/' . (string)$css;
		            $packs[$name]['resources'][] = $resource;
		        }
		    }
		}

		$baseUrl = Zend_Controller_Front::getInstance()->getRequest()->getBaseUrl();
		$ret = '';
		foreach ($packs[$name]['resources'] as $resource) {
			$ret .= '<style media="'.$packs[$name]['media'].'" type="text/css">@import url(\''.Href::apply(';/resources/' . $resource).'\');</style>';
		}
		return $ret;
	}

}

?>