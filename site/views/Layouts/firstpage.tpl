<?= $this->docType('XHTML1_STRICT') ?>
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?= $this->infos['domain'] ?>" xml:lang="<?= $this->infos['domain'] ?>">
	 <head>

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="Content-Language" content="<?= $this->infos['domain'] ?>" />
		<meta http-equiv="Content-style-type" content="text/css" />
		
		<meta name="reply-to" content="<?= $this->infos['contactmail'] ?>" />
		<meta name="rating" content="general" />
		<meta name="robots" content="index,follow" />

		<title>PimpMyV</title>
		
		<?= $this->jspack('default', $this->forcejs) ?>
		<?= $this->csspack('default', $this->forcecss) ?>

	</head>

	<body>
		
		<?= $this->layout()->content ?>

	</body>
</html>