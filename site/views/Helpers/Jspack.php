<?php

class Zend_View_Helper_Jspack {
    
    public function jspack($name = null, $force = null) {
    	if (!$name) return '';
		if ($force) $name = $force;
    	
    	$packs = array(
    		'default' => array(
    			'js/prototype/prototype-1.6.0.js',
    			'js/lib/xgviz.js'
    		)
    	);
    	
    	if (!in_array($name, array_keys($packs))) return '';

    	$baseUrl = Zend_Controller_Front::getInstance()->getRequest()->getBaseUrl();
    	$ret = '';
    	foreach ($packs[$name] as $resource) {
    		$ret .= '<script type="text/javascript" src="'.$baseUrl.'/resources/'.$resource.'"></script>';
    	}
    	return $ret;
    }
    
}

?>