<?php

class Zend_View_Helper_T {
    
    public function t($key = null) {
		$K = Kernel::getInstance();
		
    	if (!$key) throw new Exception('You have to provide a key');

		return $K->i18n->_($key);
    }
    
}

?>